(define-module (jeko-packages)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages code)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix packages)
  #:use-module (guix build-system guile)
  #:use-module (guix build-system emacs)
  #:use-module (guix build utils)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

(define-public guile-gunit64
  (let ([commit "094c17a42b0320085ecd4c0ba69462da317e6bdc"]
	[hash "0d19xpai462d6g89r89inzjb31cp4raa4x0hfdx0vpa787733qk2"]
	[revision "0"])
    (package
      (name "guile-gunit64")
      (version (git-version "0.0.1" revision commit))
      (home-page "https://framagit.org/jeko/guile-gunit64")
      (source
       (origin
	 (method git-fetch)
	 (uri
	  (git-reference
	   (url home-page)
	   (commit commit)))
	 (sha256 (base32 hash))
	 (file-name (git-file-name name version))))
      (build-system guile-build-system)
      (inputs (list guile-3.0))
      (propagated-inputs `())
      (arguments
       '(#:phases
	 (modify-phases %standard-phases
	   (add-after 'unpack 'move-src-files
	     (lambda _
	       (delete-file "guix.scm")
	       #t)))))
      (synopsis "SRFI64 runner & co to manage test execution")
      (description "SRFI64 runner & co to manage test execution")
      (license license:gpl3+))))

(define-public emacs-geiser-gunit64
  (let ([hash "13fggrm94y5y0vic7s5f51wnnjbb392767vnkkzsx4axadqav7lj"]
	[commit "7a873572cf39cef85a7eaeb04e4c4fc92ca9fa35"]
	[revision "0"])
    (package
      (name "emacs-geiser-gunit64")
      (version (git-version "0.0.1" revision commit))
      (home-page "https://framagit.org/jeko/geiser-gunit64")
      (source
       (origin
	 (method git-fetch)
	 (uri
	  (git-reference
	   (url "https://framagit.org/jeko/geiser-gunit64")
	   (commit commit)))
	 (sha256 (base32 hash))
	 (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs (list emacs-geiser guile-gunit64))
      (synopsis "Gunit64 minor mode for Emacs powered by Geiser")
      (description "Trigger Gunit64 tests in Emacs thanks to keystrokes.")
      (license license:gpl3+))))

(define-public emacs-redacted
  (package
    (name "emacs-redacted")
    (version "20220108.1037")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/bkaestner/redacted.el.git")
             (commit "c4ea6cbffda9c67af112f25b2db2843aa4abce85")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1p693dbnx2vq2a7fps28nxd37jcdw4iba2fkac6qf02sqa2xwxk9"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/bkaestner/redacted.el")
    (synopsis "Obscure text in buffer")
    (description
     "This package provides `redacted-mode', a buffer-local mode that uses
`buffer-display-table to replace the displayed glyphs with Unicode blocks.")
    (license license:gpl3+)))

(define-public artanis-1.0
  (package (inherit artanis)
    (version "1.0.0")))
